#!/usr/bin/bash

PATH="../k2raster-2022/dataset/temporal/Temporal/NASA/NLDAS_FORA0125_H/"
RASTERS=2664

echo "H_0 row major"

./entropy_zero $PATH"APCP_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero $PATH"CAPE_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero $PATH"CONVfrac_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero $PATH"DLWRF_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero $PATH"DSWRF_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero $PATH"PEVAP_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero $PATH"PRES_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero $PATH"SPFH_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero $PATH"TMP_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero $PATH"UGRD_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero $PATH"VGRD_NLDAS_FORA0125_H.txt" $PATH $RASTERS

echo "H_0 z orden"

./entropy_zero_z $PATH"APCP_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero_z $PATH"CAPE_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero_z $PATH"CONVfrac_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero_z $PATH"DLWRF_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero_z $PATH"DSWRF_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero_z $PATH"PEVAP_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero_z $PATH"PRES_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero_z $PATH"SPFH_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero_z $PATH"TMP_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero_z $PATH"UGRD_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./entropy_zero_z $PATH"VGRD_NLDAS_FORA0125_H.txt" $PATH $RASTERS

for ((K=1;K<7;K++));
do
	echo "H_"$K" z orden"
	./entropy_k_z $PATH"APCP_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_k_z $PATH"CAPE_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_k_z $PATH"CONVfrac_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_k_z $PATH"DLWRF_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_k_z $PATH"DSWRF_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_k_z $PATH"PEVAP_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_k_z $PATH"PRES_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_k_z $PATH"SPFH_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_k_z $PATH"TMP_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_k_z $PATH"UGRD_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_k_z $PATH"VGRD_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
done

for ((K=0;K<7;K++));
do
	echo "H_"$K" cuad z orden"
	./entropy_cuad_0_z $PATH"APCP_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_cuad_0_z $PATH"CAPE_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_cuad_0_z $PATH"CONVfrac_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_cuad_0_z $PATH"DLWRF_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_cuad_0_z $PATH"DSWRF_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_cuad_0_z $PATH"PEVAP_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_cuad_0_z $PATH"PRES_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_cuad_0_z $PATH"SPFH_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_cuad_0_z $PATH"TMP_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_cuad_0_z $PATH"UGRD_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
	./entropy_cuad_0_z $PATH"VGRD_NLDAS_FORA0125_H.txt" $PATH $RASTERS $K
done