/*
 * Entropia de celdas en orden k manual en z orden
 */
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <string>
#include <sstream>

#include "utilities.hpp"

using namespace std;

// Recibe un string con numeros separados por un espacio, y los vierte en el vector
void get_vector_list(vector<int> & datas, string list){
	stringstream check1(list);
	string intermediate;
	
	while(getline(check1, intermediate, ' ')){
		datas.push_back(stoi(intermediate));
	}
}

double get_entropy(const vector<int> & datas, size_t k){
	size_t n = datas.size();
	string n_a;
	double entropy = 0.0;
	string key;
	
	map<string, string> t_s;
	
	for(size_t i = 0; i < n-k; i++){
		key = "";
		for(size_t j = 0; j < k; j++){
			key += to_string(datas[i+j]) + " ";
		}
		
		if(!t_s.count(key)){
			t_s[key] = i+k != n ? to_string(datas[i+k])+" " : "";
		}else{
			t_s[key] += i+k != n ? to_string(datas[i+k])+" " : "";
		}
	}
	
	map<string, string>::iterator it;
	size_t counter = 0;
	for (it = t_s.begin(); it != t_s.end(); it++){
		key = it->first;
		n_a = it->second;
		
		vector<int> t_s_vector;
		get_vector_list(t_s_vector, n_a);
		
		double factor = (double)t_s_vector.size() / (double)n;
		double h_0 = h0(t_s_vector);
		entropy += factor * h_0;
		
	}
	
	return entropy;
}



int main(int argc, char ** argv){
	if(argc != 5){
		cout << "usage: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters> <k>" << endl;
		return -1;
	}
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_elements = atoi(argv[3]);
	size_t k = atoi(argv[4]);
	
	vector<int> datas;
	read_z_order(datas, inputs_file, inputs_path, n_elements);
	
	double entropy = get_entropy(datas, k);
	
	cout << inputs_file << "\t" << entropy << endl;
	
	return 0;
}