CPP=g++

OBJECTS=utilities.o
		
BINS=entropy_zero entropy_zero_z entropy_k_z entropy_cuad_0_z
		
CPPFLAGS=-std=c++11 -O3 -DNDEBUG
DEST=.

%.o: %.c
	$(CPP) $(CPPFLAGS) -c $< -o $@

all: clean bin

bin: $(OBJECTS) $(BINS)

entropy_zero:
	$(CPP) $(CPPFLAGS) -o $(DEST)/entropy_zero entropy_zero.cpp $(OBJECTS)

entropy_zero_z:
	$(CPP) $(CPPFLAGS) -o $(DEST)/entropy_zero_z entropy_zero_z.cpp $(OBJECTS) 
	
entropy_k_z:
	$(CPP) $(CPPFLAGS) -o $(DEST)/entropy_k_z entropy_k_z.cpp $(OBJECTS) 
	
entropy_cuad_0_z:
	$(CPP) $(CPPFLAGS) -o $(DEST)/entropy_cuad_0_z entropy_cuad_0_z.cpp $(OBJECTS) 

clean:
	rm -f $(OBJECTS) $(BINS)
	cd $(DEST); rm -f *.a $(BINS)
