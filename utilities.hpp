
#ifndef UT_HPP
#define UT_HPP

#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <cmath>

using namespace std;

unsigned long interleave_uint32_with_zeros(unsigned int input);

unsigned long get_zorder(unsigned int n_rows, unsigned int n_cols);

unsigned int get_k(unsigned int n_rows, unsigned int n_cols);

bool read_row_major(vector<int> & datas, string inputs_file, string inputs_path, size_t n_elements);

bool read_z_order(vector<int> & datas, string inputs_file, string inputs_path, size_t n_elements);

double h0(const vector<int> & datas);

#endif