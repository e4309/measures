/*
 * Entropia de celdas en orden cero manual en z orden
 */
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <cmath>

#include "utilities.hpp"

using namespace std;

double get_entropy(const vector<int> & datas){
	size_t n = datas.size();
	int value;
	size_t n_a;
	double entropy = 0.0;
	
	map<int, size_t> frequencies;
	for(size_t i = 0; i < n; i++){
		frequencies[datas[i]]++;
	}
	
	map<int, size_t>::iterator it;
	size_t counter = 0;
	for (it = frequencies.begin(); it != frequencies.end(); it++){
		value = it->first;
		n_a = it->second;
		
		double factor = (double)n_a / (double)n;
		double logar = log2(1 / factor);
		entropy += factor * logar;
		
		counter += n_a;
	}
	
	if(n != counter){
		cout << "An error ocurred!" << endl;
		return 0.0;
	}
	
	return entropy;
}



int main(int argc, char ** argv){
	if(argc != 4){
		cout << "usage: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters>" << endl;
		return -1;
	}
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_elements = atoi(argv[3]);
	
	vector<int> datas;
	read_z_order(datas, inputs_file, inputs_path, n_elements);
	
	double entropy = get_entropy(datas);
	
	cout << inputs_file << "\t" << entropy << endl;
	
	return 0;
}