/*
 * Entropia de celdas en orden cero manual en row major orden
 */
#include <fstream>
#include <iostream>
#include <vector>


#include "utilities.hpp"

using namespace std;

double get_entropy(const vector<int> & datas){
	return h0(datas);
}

int main(int argc, char ** argv){
	if(argc != 4){
		cout << "usage: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters>" << endl;
		return -1;
	}
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_elements = atoi(argv[3]);
	
	vector<int> datas;
	read_row_major(datas, inputs_file, inputs_path, n_elements);
	
	double entropy = get_entropy(datas);
	
	cout << inputs_file << "\t" << entropy << endl;
	
	return 0;
}