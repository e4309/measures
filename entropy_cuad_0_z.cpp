/*
 * Entropia de cuadros en orden cero manual en z orden
 */
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <string>

#include "utilities.hpp"

using namespace std;

double get_entropy(const vector<int> & datas, size_t exp){
	size_t C = pow(pow(2, exp), 2);
	size_t n = datas.size();
	size_t n_a;
	double entropy = 0.0;
	string key;
	
	map<string, size_t> frequencies;
	for(size_t i = 0; i < n-C; i+=C){
		key = "";
		for(size_t j = 0; j < C; j++){
			key += to_string(datas[i+j]) + " ";
		}
		
		frequencies[key]++;
	}
	
	map<string, size_t>::iterator it;
	size_t counter = 0;
	for (it = frequencies.begin(); it != frequencies.end(); it++){
		key = it->first;
		n_a = it->second;
		
		double factor = (double)n_a / (double)n;
		double logar = log2(1 / factor);
		entropy += factor * logar;
		
		counter += n_a;
	}
	
	return entropy;
}



int main(int argc, char ** argv){
	if(argc != 5){
		cout << "usage: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters> <exp>" << endl;
		return -1;
	}
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_elements = atoi(argv[3]);
	size_t exp = atoi(argv[4]);
	
	vector<int> datas;
	read_z_order(datas, inputs_file, inputs_path, n_elements);
	
	double entropy = get_entropy(datas, exp);
	
	cout << inputs_file << "\t" << entropy << endl;
	
	return 0;
}